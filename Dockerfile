# syntax=docker/dockerfile:experimental

FROM alpine:3.11 as builder

RUN apk --no-cache upgrade \
    && apk add --no-cache \
    cmake \
    cython \
    g++ \
    linux-headers \
    make \
    ninja \
    py3-numpy \
    py3-numpy-dev \
    py3-setuptools \
    py3-scipy \
    python3 \
    python3-dev \
    wget 

RUN apk add --no-cache \
        --repository http://dl-cdn.alpinelinux.org/alpine/edge/community \
        hdf5-dev

RUN apk add --no-cache \
    openssl-dev \
    blas-dev \
    lapack-dev

# python build toold
RUN python3 -m pip install --upgrade \
    pip \
    scikit-build \
    wheel

# build python wheels
WORKDIR /build/
COPY ./requirements.txt .
RUN --mount=type=cache,target=/root/.cache/pip \
    python3 -m pip wheel \
        --wheel-dir=/build/wheelhouse \
        --no-deps \
	--no-build-isolation \
        -r requirements.txt

WORKDIR /build/spatial/
RUN ls
# build clibs
RUN --mount=type=cache,target=/build/spatial/build \
 wget https://github.com/libspatialindex/libspatialindex/releases/download/1.9.3/spatialindex-src-1.9.3.tar.gz \
 && tar xf spatialindex-src-1.9.3.tar.gz \
 && cd spatialindex-src-1.9.3 \
 && mkdir build \
 && cd build \
 && cmake -DCMAKE_INSTALL_PREFIX=/libspatial/ .. \
 && make 

WORKDIR /build/spatial/spatialindex-src-1.9.3/build
RUN make install


FROM busybox
WORKDIR /out/
COPY --from=builder /build/wheelhouse/ /out/wheels/
COPY --from=builder /libspatial/ /out/libs/libspatial/
RUN ls /out/wheels

